// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

const config = {
  title: 'Qapodaster',
  tagline: 'Заметки из личного опыта',
  url: 'https://qapodaster.gitlab.io/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/qapo_favicon.ico',
  organizationName: 'eriknas',
  projectName: 'Qapodaster',

  presets: [
    [
      'classic',
      ({
        docs: false,
        blog: {
          path: './blog',
          routeBasePath: '/',
          showReadingTime: true,
          blogSidebarTitle: 'Все записи',
//          editUrl:
//            'https://github.com/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    ({
      navbar: {
        title: 'Qapodaster. Заметки из личного опыта',
        logo: {
          alt: 'Qapodaster Logo',
          src: 'img/qapo_logo.png',
        },
        items: [
//          {
//            to: '/about',
//            label: 'Обо мне',
//            position: 'left'
//          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Контакты',
            items: [
              {
                label: 'Telegram',
                href: 'https://t.me/eriknas',
              },
              {
                label: 'Instagram',
                href: 'https://instagram.com/eriknas63',
              },
            ],
          },
          {
            title: 'Еще',
            items: [
              {
                label: 'Обо мне',
                to: '/about',
              },
              {
                label: 'Gitlab',
                href: 'https://github.com/qapodaster/',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Qapodaster.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
