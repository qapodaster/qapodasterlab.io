---
slug: json-java-blog-post
title: Сравнение данных json в Java
tags: [json, java]
---
При тестировании Rest API часто возникает задача сравнения ответа сервера с эталонным значением json.
Это можно сделать несколькими способами и каждый имеет плюсы и минусы.

<!--truncate-->

### 0. Входные данные

Json это текстовый формат данных. Для примера будем использовать 2 переменные со строками формата Json.

    String json1 = "{\"name\":\"Alysa\"}";
    String json2 = "{\"name\":\"Siri\"}";

### 1. Сравнение строк 

Json это определенный формат строки. Поэтому мы можем просто сравнить строки.

    boolean result = json1.equals(json2);

Плюсы - самый простой и понятный вариант.

Минусы - если в json будет изменен порядок элементов, то они уже не будут равны, хотя являются
эквивалентными.

### 2. Сравнение c помощью JSONObject

JSONObject (библиотека [org.json](https://mvnrepository.com/artifact/org.json/json))
позволяет работать с json как с объектами. 

Воспользуемся его методом similar.

    JSONObject jo1 = new JSONObject(json1);
    JSONObject jo2 = new JSONObject(json2);

    boolean result = jo1.similar(jo2);

Плюсы - не важен порядок.

Минусы - возможен вызов исключения при преобразовании в JSONObject, если на вход попадут некорректные данные.

### 3. Сравнение с помощью Jackson

ObjectMapper (библиотека [Jackson Databind](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind))
позволяет создать объекты на основе json и сравнивать их.


    ObjectMapper mapper = new ObjectMapper();

    JsonNode tree1 = mapper.readTree(json1);
    JsonNode tree2 = mapper.readTree(json2);

    boolean result3 = tree1.equals(tree2);

Плюсы - не важен порядок.

Минусы - readTree придется заворачивать в блок try...
Подробнее можно почитать на [Baeldung](https://www.baeldung.com/jackson-compare-two-json-objects)

### 4. Сравнение с помощью Gson

Gson (библиотека [Gson](https://mvnrepository.com/artifact/com.google.code.gson/gson))
    позволяет создать объекты на основе json и сравнивать их.

    Gson gson = new GsonBuilder().create();

    JsonElement je1 = gson.toJsonTree(json1);
    JsonElement je2 = gson.toJsonTree(json2);

    boolean result4 = je1.equals(je2);

Плюсы - не важен порядок.

Минусы - библиотека создана для сериализации/десериализации объектов Java.

Использование ее исключительно для сравнения сомнительная затея.
Подробнее можно почитать на [Github](https://github.com/google/gson)

### Вместо заключения

Это не все варианты, но самые популярные. 

Если вы знаете способ лучше, то напишите, всегда открыт к общению.