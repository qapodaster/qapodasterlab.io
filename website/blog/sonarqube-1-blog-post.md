---
slug: sonarqube-1-blog-post
title: Разворачиваем SonarQube с помощью Docker
tags: [sonarqube, docker, docker-compose]
---

SonarQube не самый популярный инструмент у автотестеров, хотя позволяет улучшить читаемость кода, найти оптимальные языковые конструкции и выявить использование устаревших методов.

Про сонар вы всегда сможете почитать самостоятельно, а я хочу рассказать как его запустить не вчитываясь в тонну информации из разных источников.

<!--truncate-->

## Создаем описание контейнеров
docker-compose.yml
```yaml
version: "3"

services:
  sonarqube:
    image: sonarqube:8-community
    depends_on:
      - init
    expose:
      - 9000
    ports:
      - "127.0.0.1:9000:9000"
    networks:
      - sonarnet
    environment:
      - sonar.jdbc.url=jdbc:postgresql://db:5432/sonar
      - sonar.jdbc.username=sonar
      - sonar.jdbc.password=sonar
    volumes:
      - sonarqube_conf:/opt/sonarqube/conf
      - sonarqube_data:/opt/sonarqube/data
      - sonarqube_extensions:/opt/sonarqube/extensions
      - sonarqube_bundled-plugins:/opt/sonarqube/lib/bundled-plugins

  db:
    image: postgres:13
    networks:
      - sonarnet
    environment:
      - POSTGRES_USER=sonar
      - POSTGRES_PASSWORD=sonar
    volumes:
      - postgresql:/var/lib/postgresql
      - postgresql_data:/var/lib/postgresql/data

  init:
    image: bash
    privileged: true
    user: root
    volumes:
      - ./init.sh:/mnt/init.sh
    command: ["sh", "-e", "/mnt/init.sh"]

networks:
  sonarnet:

volumes:
  sonarqube_conf:
  sonarqube_data:
  sonarqube_extensions:
  sonarqube_bundled-plugins:
  postgresql:
  postgresql_data:

```

Это yaml, который будет запускать образ Сонара и образ БД, который использует сам Сонар.
Логин и пароль указаны тестовые, если вы боретесь за безопасность или сервис будет доступен извне, то лучше поменять его.

## Дополнительная настройка
init.sh
```bash
sysctl -w vm.max_map_count=524288
sysctl -w fs.file-max=131072
```

Это короткий скрипт в 2 строчки, который изменяет параметры образа контейнера. Без инициализации контейнер с сонаром будет быстро умирать. Наверняка эти 2 строчки можно затолкать в Docker-compose.yml, если вы знаете как, то напишите.

## Запуск контейнеров

```bash
docker-compose -f /docker-compose.yml up -d --force-recreate --build
```

Файл init.sh должен лежать рядом с docker-compose.yml

Давайте проясним по ключам запуска.

`docker-compose` утилита управления докер-контейнерами

`-f` флаг для указания пути к файлу yaml

`/docker-compose.yml` путь к файлу yaml

`up` команда создания и запуска контейнеров

`-d` автономный режим: запуск контейнеров в фоновом режиме, распечатать новые имена контейнеров

`--force-recreate` создать контейнеры заново, даже если их конфигурация и образ не изменился

`-build` флаг для создания или пересоздания сервисов

## Дальнейшее использование

Если все делали по инструкции, то никаких дополнительных проблем у вас возникнуть не должно. Можете переходить на веб интерфейс (например [localhost:9000](http://localhost:9000) ) и создавать свои проекты.

В следующем посте расскажу как настроить SonarScanner для gradle-проекта и как подружить Sonar и Gitlab.